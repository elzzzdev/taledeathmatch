// Fill out your copyright notice in the Description page of Project Settings.

#include "TDMWeapon.h"

#include "Core/TDMCharacter.h"
#include "Kismet/GameplayStatics.h"

ATDMWeapon::ATDMWeapon()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMesh->SetupAttachment(RootComponent);
	SkeletalMesh->SetCollisionProfileName(FName("NoCollision"));
}

void ATDMWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDMWeapon::Shoot(const FVector& ViewpointForwardVector)
{
	if(HasAmmo())
	{
		CurrentAmmo--;
		FireMuzzleFlash();
		Server_FireMuzzleFlash();
		Server_HitTrace(ViewpointForwardVector);
	}
}


void ATDMWeapon::Reload()
{
	CurrentAmmo = MagazineCapacity;
}

bool ATDMWeapon::HasAmmo() const
{
	return CurrentAmmo > 0;
}

bool ATDMWeapon::IsMagazineFull() const
{
	return CurrentAmmo == MagazineCapacity;
}

void ATDMWeapon::BeginPlay()
{
	Super::BeginPlay();
	QueryParams.AddIgnoredActor(this);
	CurrentAmmo = MagazineCapacity;
}

void ATDMWeapon::Server_HitTrace_Implementation(const FVector& ViewpointForwardVector)
{
	const FVector MuzzleLocation = SkeletalMesh->GetSocketLocation(FName("Muzzle"));
	FVector TraceStart = MuzzleLocation;
	FVector TraceEnd = ViewpointForwardVector * 7000 + MuzzleLocation;
	FHitResult Hit;
	GetWorld()->LineTraceSingleByChannel(Hit, TraceStart, TraceEnd, ECC_Pawn, QueryParams);

	// DrawDebugLine(GetWorld(), TraceStart, TraceEnd, Hit.bBlockingHit ? FColor::Blue : FColor::Red, false, 5.0f, 0, 3.0f);

	if (Hit.bBlockingHit && IsValid(Hit.GetActor()))
	{
		ATDMCharacter* Target = Cast<ATDMCharacter>(Hit.GetActor());
		if (Target)
		{
			Target->DealDamage(15);
		}
	}
}

void ATDMWeapon::FireMuzzleFlash() const
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MuzzleFlashEffect, SkeletalMesh->GetSocketLocation(FName("Muzzle")), SkeletalMesh->GetSocketRotation(FName("Muzzle")), FVector(0.1));
}

void ATDMWeapon::Server_FireMuzzleFlash_Implementation() const
{
	NetMulticast_FireMuzzleFlash();
}

void ATDMWeapon::NetMulticast_FireMuzzleFlash_Implementation() const
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), MuzzleFlashEffect, SkeletalMesh->GetSocketLocation(FName("Muzzle")), SkeletalMesh->GetSocketRotation(FName("Muzzle")), FVector(0.1));
}
