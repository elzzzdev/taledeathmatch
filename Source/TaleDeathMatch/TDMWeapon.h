// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDMWeapon.generated.h"

UCLASS()
class TALEDEATHMATCH_API ATDMWeapon : public AActor
{
	GENERATED_BODY()

public:
	ATDMWeapon();

	virtual void Tick(float DeltaTime) override;

	void Shoot(const FVector& ViewpointForwardVector);

	void Reload();

	bool HasAmmo() const;

	bool IsMagazineFull() const;

protected:
	virtual void BeginPlay() override;

	UFUNCTION(Server, Reliable)
	void Server_HitTrace(const FVector& ViewpointForwardVector);

	void FireMuzzleFlash() const;

	UFUNCTION(Server, Reliable)
	void Server_FireMuzzleFlash() const;

	UFUNCTION(NetMulticast, Reliable)
	void NetMulticast_FireMuzzleFlash() const;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UParticleSystem* MuzzleFlashEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	int32 MagazineCapacity;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	int32 CurrentAmmo = 0;

	FCollisionQueryParams QueryParams;
};
