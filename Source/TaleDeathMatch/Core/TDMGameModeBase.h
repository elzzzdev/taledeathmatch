// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDMGameModeBase.generated.h"

UCLASS()
class TALEDEATHMATCH_API ATDMGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDMGameModeBase();

	void KillAndRestartPlayer(AController* Player);

protected:
	virtual void BeginPlay() override;
};
