// Fill out your copyright notice in the Description page of Project Settings.

#include "TDMCharacter.h"
#include "Engine/LocalPlayer.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Controller.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "TDMGameModeBase.h"
#include "Components/TimelineComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"
#include "TaleDeathMatch/TDMWeapon.h"

ATDMCharacter::ATDMCharacter()
{
	bReplicates = true;
	PrimaryActorTick.bCanEverTick = true;
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	if (UCharacterMovementComponent* CharacterMovementComponent = GetCharacterMovement())
	{
		CharacterMovementComponent->bOrientRotationToMovement = true;
		CharacterMovementComponent->RotationRate = FRotator(0.0f, 500.0f, 0.0f);
		CharacterMovementComponent->JumpZVelocity = 400.f;
		CharacterMovementComponent->AirControl = 0.35f;
		CharacterMovementComponent->MinAnalogWalkSpeed = 0.f;
		CharacterMovementComponent->BrakingDecelerationWalking = 2000.f;
		CharacterMovementComponent->BrakingDecelerationFalling = 1500.f;
		CharacterMovementComponent->SetCrouchedHalfHeight(75.f);
		CharacterMovementComponent->GetNavAgentPropertiesRef().bCanCrouch = true;
	}

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f;
	CameraBoom->bUsePawnControlRotation = true;

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;
}

void ATDMCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ShootCooldownTimeline.TickTimeline(DeltaTime);
}

void ATDMCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ATDMCharacter::Move);
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ATDMCharacter::Look);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ATDMCharacter::Jump);
		// EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ATDMCharacter::StopJumping);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Started, this, &ATDMCharacter::Sprint);
		EnhancedInputComponent->BindAction(SprintAction, ETriggerEvent::Completed, this, &ATDMCharacter::StopSprint);
		EnhancedInputComponent->BindAction(CrouchAction, ETriggerEvent::Started, this, &ATDMCharacter::Crouch, true);
		EnhancedInputComponent->BindAction(CrouchAction, ETriggerEvent::Completed, this, &ATDMCharacter::UnCrouch, true);
		EnhancedInputComponent->BindAction(AimAction, ETriggerEvent::Started, this, &ATDMCharacter::Aim, false);
		EnhancedInputComponent->BindAction(AimAction, ETriggerEvent::Completed, this, &ATDMCharacter::StopAim, false);
		EnhancedInputComponent->BindAction(ShootAction, ETriggerEvent::Started, this, &ATDMCharacter::PreShoot);
		EnhancedInputComponent->BindAction(ShootAction, ETriggerEvent::Completed, this, &ATDMCharacter::StopShoot);
		EnhancedInputComponent->BindAction(ReloadAction, ETriggerEvent::Started, this, &ATDMCharacter::Reload);
		EnhancedInputComponent->BindAction(SelectMainSlotAction, ETriggerEvent::Started, this, &ATDMCharacter::SelectMainGun);
		EnhancedInputComponent->BindAction(SelectSecondarySlotAction, ETriggerEvent::Started, this, &ATDMCharacter::SelectSecondaryGun);
	}
}

bool ATDMCharacter::CanJumpInternal_Implementation() const
{
	return !bWantToShoot && !bIsAiming && Super::CanJumpInternal_Implementation();
}

bool ATDMCharacter::CanSprint() const
{
	return !bWantToShoot && !bIsAiming && !bReloading;
}

bool ATDMCharacter::CanShoot() const
{
	return GetEquippedGun() != nullptr && GetEquippedGun()->HasAmmo() && !bReloading && !bSwitchingGun;
}

ATDMWeapon* ATDMCharacter::GetEquippedGun() const
{
	return bMainWeaponInUse ? MainGun : SecondaryGun;
}

void ATDMCharacter::ResetStats()
{
	Health = MaxHealth;
	MainGun->Reload();
	SecondaryGun->Reload();
}

void ATDMCharacter::DealDamage_Implementation(uint32 Damage)
{
	Health -= Damage;
	if(Health <= 0)
	{
		if(ATDMGameModeBase* GM = Cast<ATDMGameModeBase>(UGameplayStatics::GetGameMode(GetWorld())))
		{
			GM->KillAndRestartPlayer(GetController());
		}
	}
}

void ATDMCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (const APlayerController* PC = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PC->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}

	if (UCharacterMovementComponent* CharacterMovementComponent = GetCharacterMovement())
	{
		CharacterMovementComponent->MaxWalkSpeed = WalkSpeed;
		CharacterMovementComponent->MaxWalkSpeedCrouched = WalkSpeedCrouched;
	}

	if (ShootCooldownCurve)
	{
		FOnTimelineFloat OnShootCooldownCallback;
		FOnTimelineEventStatic OnShootCooldownFinishedCallback;

		OnShootCooldownCallback.BindUFunction(this, FName("ShootCooldownCallback"));
		ShootCooldownTimeline.AddInterpFloat(ShootCooldownCurve, OnShootCooldownCallback);

		OnShootCooldownFinishedCallback.BindUFunction(this, FName("ShootCooldownFinishedCallback"));
		ShootCooldownTimeline.SetTimelineFinishedFunc(OnShootCooldownFinishedCallback);
	}

	Server_CreateWeapons();
}

void ATDMCharacter::Crouch(bool bClientSimulation)
{
	Super::Crouch(bClientSimulation);
	Server_SetCrouchButtonDown(CanCrouch());
}

void ATDMCharacter::UnCrouch(bool bClientSimulation)
{
	Super::UnCrouch(bClientSimulation);
	Server_SetCrouchButtonDown(false);
}

void ATDMCharacter::Server_SetCrouchButtonDown_Implementation(const bool isCrouchButtonDown)
{
	bCrouchButtonDown = isCrouchButtonDown;
}

void ATDMCharacter::Move(const FInputActionValue& Value)
{
	const FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void ATDMCharacter::Look(const FInputActionValue& Value)
{
	const FVector2D LookAxisVector = Value.Get<FVector2D>();
	if (Controller != nullptr)
	{
		AddControllerYawInput(LookAxisVector.X);
		AddControllerPitchInput(LookAxisVector.Y);
	}
}

void ATDMCharacter::Aim(bool bPreShooting)
{
	if(!bPreShooting)
	{
		bIsAiming = true;
		Server_SetIsAiming(true);
	}

	bUseControllerRotationYaw = true;
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeedAimed;

	CameraBoom->TargetArmLength = 250.0f;
	FollowCamera->SetRelativeLocation(FVector3d(0.f, 50.f, 0.f));

	Server_Aim();
}

void ATDMCharacter::StopAim(bool bPostShooting)
{
	if(bIsAiming && bPostShooting) return;
	bIsAiming = false;
	Server_SetIsAiming(false);
	if(bWantToShoot) return;

	bUseControllerRotationYaw = false;
	GetCharacterMovement()->MaxWalkSpeed = bWantToSprint ? SprintSpeed : WalkSpeed;

	CameraBoom->TargetArmLength = 400.0f;
	FollowCamera->SetRelativeLocation(FVector3d(0.f, 0.f, 0.f));

	Server_StopAim();
}

void ATDMCharacter::Server_Aim_Implementation()
{

	bUseControllerRotationYaw = true;
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeedAimed;
}

void ATDMCharacter::Server_StopAim_Implementation()
{
	bUseControllerRotationYaw = false;
	GetCharacterMovement()->MaxWalkSpeed = bWantToSprint && CanSprint() ? SprintSpeed : WalkSpeed;
}

void ATDMCharacter::Server_SetIsAiming_Implementation(const bool IsAiming)
{
	bIsAiming = IsAiming;
}

void ATDMCharacter::Sprint()
{
	bWantToSprint = true;
	if(CanSprint())
	{
		GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	}

	Server_Sprint();
}

void ATDMCharacter::StopSprint()
{
	bWantToSprint = false;
	if(CanSprint())
	{
		GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	}

	Server_StopSprint();
}

void ATDMCharacter::Server_Sprint_Implementation()
{
	bWantToSprint = true;
	if(CanSprint())
	{
		GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	}
}

void ATDMCharacter::Server_StopSprint_Implementation()
{
	bWantToSprint = false;
	if(CanSprint())
	{
		GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
	}
}

void ATDMCharacter::PreShoot()
{
	bWantToShoot = true;
	Aim(true);
	Server_SetWantToShoot(true);
	Shoot();
}

void ATDMCharacter::Shoot()
{
	if (CanShoot())
	{
		GetEquippedGun()->Shoot(UKismetMathLibrary::GetForwardVector(FollowCamera->GetComponentRotation()));
	}
	ShootCooldownTimeline.PlayFromStart();
}

void ATDMCharacter::StopShoot()
{
	bWantToShoot = false;
	StopAim(true);
	Server_SetWantToShoot(false);
	ShootCooldownTimeline.Stop();
}

void ATDMCharacter::Server_SetWantToShoot_Implementation(const bool wantToShoot)
{
	bWantToShoot = wantToShoot;
}

void ATDMCharacter::ShootCooldownCallback(float Alpha)
{

}

void ATDMCharacter::ShootCooldownFinishedCallback()
{
	Shoot();
}

void ATDMCharacter::Reload()
{
	if(GetEquippedGun()->IsMagazineFull() || bReloading || bSwitchingGun) return;
	bWantToReload = true;
	Server_SetWantToReload(true);
}

void ATDMCharacter::ReloadComplete()
{
	bReloading = false;
	GetEquippedGun()->Reload();
}

void ATDMCharacter::Server_SetWantToReload_Implementation(const bool WantToReload)
{
	NetMulticast_SetWantToReload(WantToReload);
}

void ATDMCharacter::NetMulticast_SetWantToReload_Implementation(const bool WantToReload)
{
	bWantToReload = WantToReload;
}

void ATDMCharacter::SelectMainGun()
{
	if(GetEquippedGun() == MainGun) return;
	StartSwitchGun();
}

void ATDMCharacter::SelectSecondaryGun()
{
	if(GetEquippedGun() == SecondaryGun) return;
	StartSwitchGun();
}

void ATDMCharacter::StartSwitchGun()
{
	if(bReloading) return;
	bWantToSwitchGun = true;
	Server_SetWantToSwitchGun(true);
}

void ATDMCharacter::SwitchGunNotify()
{
	GetEquippedGun()->SetActorHiddenInGame(true);
	ATDMWeapon* NewGun = bMainWeaponInUse ? SecondaryGun : MainGun;
	NewGun->SetActorHiddenInGame(false);
	bMainWeaponInUse = !bMainWeaponInUse;
}

void ATDMCharacter::SwitchGunComplete()
{
	bSwitchingGun = false;
}

void ATDMCharacter::Server_SetWantToSwitchGun_Implementation(const bool WantToSwitchGun)
{
	NetMulticast_SetWantToSwitchGun(WantToSwitchGun);
}

void ATDMCharacter::NetMulticast_SetWantToSwitchGun_Implementation(const bool WantToSwitchGun)
{
	bWantToSwitchGun = WantToSwitchGun;
}

void ATDMCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATDMCharacter, bCrouchButtonDown);
	DOREPLIFETIME(ATDMCharacter, bIsAiming);
	DOREPLIFETIME(ATDMCharacter, bWantToShoot);
	DOREPLIFETIME(ATDMCharacter, bWantToSprint);
	DOREPLIFETIME(ATDMCharacter, MainGun);
	DOREPLIFETIME(ATDMCharacter, SecondaryGun);
	// DOREPLIFETIME(ATDMCharacter, EquippedGun);
	DOREPLIFETIME(ATDMCharacter, Health);
}

void ATDMCharacter::Server_CreateWeapons_Implementation()
{
	MainGun = GetWorld()->SpawnActor<ATDMWeapon>(MainGunType);
	MainGun->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, FName("hand_r_socket"));
	MainGun->SetOwner(this);

	SecondaryGun = GetWorld()->SpawnActor<ATDMWeapon>(SecondaryGunType);
	SecondaryGun->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, FName("hand_r_socket"));
	SecondaryGun->SetOwner(this);
	SecondaryGun->SetActorHiddenInGame(true);
}
