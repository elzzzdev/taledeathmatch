// Fill out your copyright notice in the Description page of Project Settings.


#include "TDMGameModeBase.h"

#include "TDMCharacter.h"
#include "GameFramework/Character.h"
#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetArrayLibrary.h"

ATDMGameModeBase::ATDMGameModeBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATDMGameModeBase::KillAndRestartPlayer(AController* Player)
{
	ATDMCharacter* Character = Cast<ATDMCharacter>(Player->GetCharacter());
	Character->ResetStats();

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), FoundActors);
	const int32 SpotIndex = FMath::RandRange(0, FoundActors.Num() - 1);
	Character->SetActorLocation(FoundActors[SpotIndex]->GetActorLocation());
}

void ATDMGameModeBase::BeginPlay()
{
	Super::BeginPlay();
}
