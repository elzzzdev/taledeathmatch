// Fill out your copyright notice in the Description page of Project Settings.


#include "TDMGameStateBase.h"

#include "TDMGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

ATDMGameStateBase::ATDMGameStateBase()
{
	bReplicates = true;
	PrimaryActorTick.bCanEverTick = true;
}

void ATDMGameStateBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	TimeLeft -= DeltaSeconds;
	if(TimeLeft <= 0)
	{
		TimeLeft = 0;
		// TODO Win condition
	}
}

float ATDMGameStateBase::GetTimeLeft() const
{
	return TimeLeft;
}

void ATDMGameStateBase::BeginPlay()
{
	Super::BeginPlay();
	TimeLeft = 300.f;
}

void ATDMGameStateBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATDMGameStateBase, TimeLeft);
}
