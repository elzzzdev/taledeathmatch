// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Character.h"
#include "TaleDeathMatch/TDMWeapon.h"
#include "TDMCharacter.generated.h"

class UTimelineComponent;
class ATDMWeapon;
class USpringArmComponent;
class UCameraComponent;
class UInputMappingContext;
class UInputAction;
struct FInputActionValue;

UCLASS()
class TALEDEATHMATCH_API ATDMCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDMCharacter();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual bool CanJumpInternal_Implementation() const override;

	UFUNCTION(BlueprintPure)
	bool CanSprint() const;

	UFUNCTION(BlueprintPure)
	bool CanShoot() const;

	UFUNCTION(BlueprintCallable)
	ATDMWeapon* GetEquippedGun() const;

	UFUNCTION(Server, Reliable)
	void DealDamage(uint32 Damage);

	void ResetStats();

	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

protected:
	virtual void BeginPlay() override;

	virtual void Crouch(bool bClientSimulation = false) override;

	virtual void UnCrouch(bool bClientSimulation = false) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(Server, Reliable)
	void Server_CreateWeapons();

	UFUNCTION(Server, Reliable)
	void Server_SetCrouchButtonDown(const bool isCrouchButtonDown);

	void Move(const FInputActionValue& Value);

	void Look(const FInputActionValue& Value);

	/* Aim */
	void Aim(bool bPreShooting = false);

	void StopAim(bool bPostShooting = false);

	UFUNCTION(Server, Reliable)
	void Server_Aim();

	UFUNCTION(Server, Reliable)
	void Server_StopAim();

	UFUNCTION(Server, Reliable)
	void Server_SetIsAiming(const bool IsAiming);

	/* Sprint */
	void Sprint();

	void StopSprint();

	UFUNCTION(Server, Reliable)
	void Server_Sprint();

	UFUNCTION(Server, Reliable)
	void Server_StopSprint();

	/* Shoot */
	void PreShoot();

	void Shoot();

	void StopShoot();

	UFUNCTION(Server, Reliable)
	void Server_SetWantToShoot(const bool wantToShoot);

	UFUNCTION()
	void ShootCooldownCallback(float Alpha);

	UFUNCTION()
	void ShootCooldownFinishedCallback();

	/* Reload */
	void Reload();

	UFUNCTION(BlueprintCallable)
	void ReloadComplete();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void Server_SetWantToReload(const bool WantToReload);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void NetMulticast_SetWantToReload(const bool WantToReload);

	/* Switch Gun */
	void SelectMainGun();

	void SelectSecondaryGun();

	void StartSwitchGun();

	UFUNCTION(BlueprintCallable)
	void SwitchGunNotify();

	UFUNCTION(BlueprintCallable)
	void SwitchGunComplete();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void Server_SetWantToSwitchGun(const bool WantToSwitchGun);

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable)
	void NetMulticast_SetWantToSwitchGun(const bool WantToSwitchGun);

	/* Components */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;

	/* Weapons */
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	ATDMWeapon* MainGun;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	ATDMWeapon* SecondaryGun;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<ATDMWeapon> MainGunType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapons, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<ATDMWeapon> SecondaryGunType;

	/* Input */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* LookAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* SprintAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* CrouchAction;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
    UInputAction* AimAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* ShootAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* ReloadAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* SelectMainSlotAction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* SelectSecondarySlotAction;

	/* Shoot Cooldown Timeline */
	FTimeline ShootCooldownTimeline;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Timeline, meta = (AllowPrivateAccess = "true"))
	UCurveFloat* ShootCooldownCurve;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ReloadMontage;

	/* Movement constants */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float WalkSpeed = 500.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float WalkSpeedCrouched = 250.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float SprintSpeed = 700.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float WalkSpeedAimed = 300.f;

	/* Movement states */
	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bCrouchButtonDown;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bIsAiming;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bWantToShoot;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bWantToSprint;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bWantToReload;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bReloading;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bWantToSwitchGun;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bSwitchingGun;

	bool bMainWeaponInUse = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Stats, meta = (AllowPrivateAccess = "true"))
	int32 MaxHealth = 100;

	UPROPERTY(Replicated, EditInstanceOnly, BlueprintReadWrite, Category = Stats, meta = (AllowPrivateAccess = "true"))
	int32 Health = 100;
};
