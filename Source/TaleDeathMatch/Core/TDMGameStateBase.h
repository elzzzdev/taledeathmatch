// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "TDMGameStateBase.generated.h"

UCLASS()
class TALEDEATHMATCH_API ATDMGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:
	ATDMGameStateBase();

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable)
	float GetTimeLeft() const;

protected:
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(Replicated)
	float TimeLeft;
};
