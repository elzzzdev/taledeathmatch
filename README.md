# TaleDeathMatch

## About
Multiplayer Shooter on Unreal Engine 5. A test assignment for 4Tale Production job application. 

## Features 

- Advanced character movement (Sprint and crouch states)
- Full movement animation coverage
- Two switchable weapons
- Ammo and health UI, session timer

During the development, preference was given to C++, all of the mechanics written with it.

## Code style
I care about the purity of the code and overall project structure, so I followed [Gamemakin Style Guide](https://github.com/Allar/ue5-style-guide) 
